import { axios } from '@/utils/axios'

export function login(params) {
  return axios({
    url: '/admin/login',
    method: 'get',
    params,
  })
}
