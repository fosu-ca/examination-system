import { axios } from '@/utils/axios'

export function getExamSetting() {
  return axios({
    url: '/student/setting',
    method: 'get',
  })
}
