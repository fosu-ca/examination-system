export const setting = {
  // 超时时间：默认10s
  timeout: 10 * 1000,
  // 接口地址
  apiUrl: import.meta.env.VITE_GLOB_API_URL,
}
