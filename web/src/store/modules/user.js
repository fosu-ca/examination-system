import { defineStore } from 'pinia'
import store from '../index'

export const useUserStore = defineStore({
  id: 'user-store',
  state: () => ({
    isAdmin: false,
    token: '',
    studentId: '',
    studentName: '',
  }),
  getters: {
    getIsAdmin() {
      return this.isAdmin
    },
    getToken() {
      return this.token
    },
    getStudentId() {
      return this.studentId
    },
    getStudentName() {
      return this.studentName
    },
  },
  actions: {
    setToken(token) {
      this.token = token
    },
    setAdmin() {
      this.isAdmin = true
    },
    setStudentId(id) {
      this.studentId = id
    },
    setStudentName(name) {
      this.studentName = name
    },
    logout() {
      this.isAdmin = false
      this.token = ''
      this.studentId = ''
      this.studentName = ''
    },
  },
})

// Need to be used outside the setup
export function useUserStoreWidthOut() {
  return useUserStore(store)
}
