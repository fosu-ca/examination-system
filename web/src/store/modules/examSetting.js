import { defineStore } from 'pinia'
import { getExamSetting } from '@/api/student/getExam'

export const useExamSetting = defineStore({
  id: 'examSetting',
  state: () => ({
    startAt: '',
    endAt: '',
    showToAdmin: false,
    randomOrder: true,
    randomOption: true,
  }),
  getters: {
    getStartAt() {
      return this.startAt
    },
    getEndAt() {
      return this.endAt
    },
    getShowToAdmin() {
      return this.showToAdmin
    },
    getRandomOrder() {
      return this.randomOrder
    },
    getRandomOption() {
      return this.randomOption
    },
  },
  actions: {
    async getExamSetting() {
      const res = await getExamSetting()
      if (res?.code === 200) {
        const { startAt, endAt, showToAdmin, randomOrder, randomOption } =
          res?.data

        this.startAt = startAt
        this.endAt = endAt
        this.showToAdmin = showToAdmin
        this.randomOrder = randomOrder
        this.randomOption = randomOption
      }
    },
  },
})
