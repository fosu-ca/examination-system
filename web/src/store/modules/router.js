import { defineStore } from 'pinia'

export const userRouteStore = defineStore({
  id: 'route',
  state: () => ({
    currentRouteName: '',
  }),
  getters: {
    getCurrentRouteName() {
      return this.currentRouteName
    },
  },
  actions: {},
})
