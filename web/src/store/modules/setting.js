import { defineStore } from 'pinia'
import designSetting from '@/setting/designSetting'
const { darkTheme, appTheme, appThemeList, menuSetting } = designSetting

export const useSetting = defineStore({
  id: 'setting',
  state: () => ({
    darkTheme,
    appTheme,
    appThemeList,
    menuSetting,
    animateSetting: 'zoom-fade',
  }),
  getters: {
    getDarkTheme() {
      return this.darkTheme
    },
    getAppTheme() {
      return this.appTheme
    },
    getAppThemeList() {
      return this.appThemeList
    },
    getAnimateSetting() {
      return this.animateSetting
    },
    getMenuSetting() {
      return this.menuSetting
    }
  },
  actions: {},
})
