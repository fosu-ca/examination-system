import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      name: 'Root',
      component: () => import('@/views/student/confirm/ConfirmView.vue'),
      meta: {
        title: 'Root',
      },
      children: [
        {
          path: 'input',
          name: 'input-sno',
          component: () => import('@/views/student/confirm/InputSno.vue')
        },
        {
          path: 'login',
          name: 'login',
          component: () => import('@/views/admin/LoginForm.vue')
        }
      ]
    },
    {
      path: '/admin',
      name: 'admin',
      component: () => import('@/views/admin/layout/LayoutIndex.vue'),
      children: [
        {
          path: 'test',
          name: 'admin-test',
          component: () => import('@/views/admin/TestView.vue')
        },
        {
          path: 'test',
          name: 'admin-home',
          component: () => import('@/views/admin/TestView.vue')
        },
        {
          path: 'test',
          name: 'admin-setting',
          component: () => import('@/views/admin/TestView.vue')
        },
        {
          path: 'test',
          name: 'admin-examinee',
          component: () => import('@/views/admin/TestView.vue')
        },
        {
          path: 'test',
          name: 'admin-paper',
          component: () => import('@/views/admin/TestView.vue')
        },
      ]
    }
  ],
  strict: true,
  scrollBehavior: () => ({ left: 0, top: 0 }),
})

export default router