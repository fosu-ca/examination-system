package util

import (
	"backServer/app/model"
	"context"
	"github.com/gogf/gf/errors/gerror"
)

func GetUserFromCtx(ctx context.Context) (*model.CtxUser, error) {
	if value := ctx.Value(model.CtxKey); value != nil {
		if ctx, ok := value.(*model.Context); ok {
			return ctx.User, nil
		}
	}
	return nil, gerror.New("获取上下文失败")
}