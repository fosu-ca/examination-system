package gfToken

import (
	"backServer/app/api"
	"github.com/goflyfox/gtoken/gtoken"
	"github.com/gogf/gf/frame/g"
)

var GfToken = &gtoken.GfToken{
	CacheMode: 			g.Cfg().GetInt8("gToken.system.CacheMode"),
	CacheKey:  			g.Cfg().GetString("gToken.system.CacheKey"),
	Timeout:          	g.Cfg().GetInt("gToken.system.Timeout"),
	MaxRefresh:       	g.Cfg().GetInt("gToken.system.MaxRefresh"),
	TokenDelimiter:   	g.Cfg().GetString("gToken.system.TokenDelimiter"),
	EncryptKey:       	g.Cfg().GetBytes("gToken.system.EncryptKey"),
	AuthFailMsg:      	g.Cfg().GetString("gToken.system.AuthFailMsg"),
	MultiLogin:			g.Cfg().GetBool("gToken.system.MultiLogin"),
	LoginPath:        	"/login",
	LogoutPath:       	"/logout",
	AuthExcludePaths: 	g.SliceStr{"/login"},
	LoginBeforeFunc: 	api.Login,
	LoginAfterFunc: 	api.LoginAfter,
}
