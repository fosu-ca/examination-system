package response

import "github.com/gogf/gf/net/ghttp"

// 封装一些成功、失败的方法

func Success(r *ghttp.Request) {
	JsonExit(r, SuccessCode, "操作成功", nil)
}

func SuccessNoExit(r *ghttp.Request) {
	Json(r, SuccessCode, "操作成功", nil)
}

func SuccessData(r *ghttp.Request, data interface{}) {
	JsonExit(r, SuccessCode, "操作成功", data)
}

func SuccessDataNoExit(r *ghttp.Request, data interface{}) {
	Json(r, SuccessCode, "操作成功", data)
}

func Fail(r *ghttp.Request, msg string) {
	JsonExit(r, ErrorCode, msg, nil)
}

func FailNoExit(r *ghttp.Request, msg string) {
	Json(r, ErrorCode, msg, nil)
}