package response

import "github.com/gogf/gf/net/ghttp"

const (
	SuccessCode int = 200
	ErrorCode   int = -1
	WarningCode int = -2
)

type Response struct {
	// 代码
	Code int `json:"code"`
	// 数据
	Data interface{} `json:"result"`
	// 消息
	Msg string `json:"message"`
	// 是否成功
	Success bool `json:"success"`
	// 结果类型
	RespType string `json:"type"`
}

// JsonExit 返回JSON数据并退出当前HTTP执行函数。
func JsonExit(r *ghttp.Request, code int, msg string, data interface{}) {
	Json(r, code, msg, data)
	r.Exit()
}

// Json 标准返回结果数据结构封装。
// 返回固定数据结构的JSON:
// code:  状态码(0:成功,<0:错误码，和http请求状态码一至);
// msg:  请求结果信息;
// data: 请求结果,根据不同接口返回结果的数据结构不同;
func Json(r *ghttp.Request, code int, msg string, data interface{}) {
	respType, success := getTypeAndSuccess(code)
	response := &Response{
		Code:    code,
		Msg:     msg,
		Data:    data,
		Success: success,
		RespType: respType,
	}
	r.Response.WriteJson(response)
}

func getTypeAndSuccess(code int) (respType string, success bool) {
	if code == SuccessCode {
		respType = "success"
		success = true
	} else if code == ErrorCode {
		respType = "error"
		success = false
	} else  if code == WarningCode {
		respType = "warning"
		success = false
	}
	return
}
