package main

import (
	_ "exam-server/boot"
	_ "exam-server/router"

	"github.com/gogf/gf/frame/g"
)

func main() {
	g.Server().Run()
}
